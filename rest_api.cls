@RestResource(urlMapping='/GamePlanSummit2018/*')
global with sharing class rest_api {
    
    private static String EVENT_NAME = 'GamePlanSummit2018';
    
    @HttpPost
    global static String doPost() {
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        
        GamePlanBLInterface gamePlan = new GamePlanBLImpl();
        
        String athleteJsonInput = req.requestBody.toString();
        String resultresponse = gamePlan.registerAttendee(athleteJsonInput);
        
        return JSON.serialize(resultresponse);
    }
    
    @HttpPut
    global static String updateAthlete() {
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        
        GamePlanBLInterface gamePlan = new GamePlanBLImpl();
        
        String athleteInfo = req.requestBody.toString();
       	
        String result = gamePlan.updateAthleteInformation(athleteInfo);
        
        return JSON.serialize(result);
    }
    
}