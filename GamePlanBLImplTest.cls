@isTest
private class GamePlanBLImplTest {
    private static Account createTestAccount() {
        String user_firstname = 'TestName';
        String user_lastname = 'TestLastName';
        String user_email = 'test@email.com';
        Account testAthlete = new Account();
        testAthlete.Name = user_firstname + ' ' + user_lastname;
        testAthlete.Email__c = user_email;
        insert testAthlete;
        return testAthlete;
    }
    
    private static Events__c createTestEvent() {
        Events__c testEvent = new Events__c();
        testEvent.Name = 'GamePlanSummit2018';
        testEvent.Maximum_Attendees__c = 160;
        insert testEvent;
        return testEvent;
    }
    
    private static String creaTestJsonInput() {
        JSONGenerator gen = JSON.createGenerator(true);    
        gen.writeStartObject();
        gen.writeStringField('user_email', 'test@email.com');
        gen.writeStringField('user_firstname', 'TestName');
        gen.writeStringField('user_lastname', 'TestLastName');
        gen.writeStringField('status', 'Test Summit Status');
        gen.writeStringField('accessibility_service_animal','Test Accessibility Service Animal');
        gen.writeStringField('accessibility_transfer_easily','Test Accessibility Transfer Easily');
        gen.writeStringField('accessibility_sport_assistant','Test Accessibility Sport Assistant');
        gen.writeStringField('accessibility_requirements','Test Accessibility Requirements');
        gen.writeStringField('accommodation_yes_text','Test Accomodation Yest Text');
        gen.writeStringField('accommodation','Test My Accomodation');
        gen.writeStringField('dietary_other_text','Test Dietary Other Text');
        gen.writeStringField('dietary','Test Dietary');
        gen.writeStringField('breakout_1','Test Breakout One');
        gen.writeStringField('breakout_2','Test Breakout Two');
        gen.writeStringField('breakout_3','Test Breakout Three');
        gen.writeStringField('breakout_3','Test Breakout Three');
        gen.writeStringField('accessibility_transfer_easily_elaborate','Test Accessibility');
        gen.writeStringField('event_name','GamePlanSummit2018');
        gen.writeEndObject();    
        return gen.getAsString();
    }
    
    private static String creaTestJsonNewInput() {
        JSONGenerator gen = JSON.createGenerator(true);    
        gen.writeStartObject();
        gen.writeStringField('user_email', 'test123@email.com');
        gen.writeStringField('user_firstname', 'TestName123');
        gen.writeStringField('user_lastname', 'TestLastName123');
        gen.writeStringField('status', 'Test Summit Status');
        gen.writeStringField('accessibility_service_animal','Test Accessibility Service Animal');
        gen.writeStringField('accessibility_transfer_easily','Test Accessibility Transfer Easily');
        gen.writeStringField('accessibility_sport_assistant','Test Accessibility Sport Assistant');
        gen.writeStringField('accessibility_requirements','Test Accessibility Requirements');
        gen.writeStringField('accommodation_yes_text','Test Accomodation Yest Text');
        gen.writeStringField('accommodation','Test My Accomodation');
        gen.writeStringField('dietary_other_text','Test Dietary Other Text');
        gen.writeStringField('dietary','Test Dietary');
        gen.writeStringField('breakout_1','Test Breakout One');
        gen.writeStringField('breakout_2','Test Breakout Two');
        gen.writeStringField('breakout_3','Test Breakout Three');
        gen.writeStringField('breakout_3','Test Breakout Three');
        gen.writeStringField('accessibility_transfer_easily_elaborate','Test Accessibility');
        gen.writeStringField('event_name','GamePlanSummit2018');
        gen.writeEndObject();    
        return gen.getAsString();
    }
    
    @testSetup
    static void gameplanTestSetup() {
        Account testAccount = createTestAccount();
        Events__c testEvent = createTestEvent();
    }
    
    private static void athleteRegistered() {
        Account testAthlete = [SELECT Id FROM Account LIMIT 1];
        Events__c testEvent = [SELECT Id FROM Events__c LIMIT 1];
        Attendee__c testAttendee = new Attendee__c(Account__c = testAthlete.Id, Event__c = testEvent.Id);
        insert testAttendee;
    }
    
    @isTest
    static void businessLogicAthleteEventRegistration() {
        GamePlanBLInterface testGamePlan = new GamePlanBLImpl();
        String athleteJsonInput = creaTestJsonInput();
        String testResult = testGamePlan.registerAttendee(athleteJsonInput);
        List<Attendee__c> resultAttendee = [SELECT ID FROM Attendee__c];
		System.debug(resultAttendee);
    }
    
    @isTest
    static void businessLogicAthleteEventCreateNewAthleteAndRegister() {
        GamePlanBLInterface testGamePlan = new GamePlanBLImpl();
        String athleteJsonInput = creaTestJsonNewInput();
        String testResult = testGamePlan.registerAttendee(athleteJsonInput);
        List<Attendee__c> resultAttendee = [SELECT ID FROM Attendee__c];
		System.debug(resultAttendee);
    }
    
    @isTest
    static void businessLogicAthleteExistAnAlreadyRegistered() {
        athleteRegistered();
        List<Attendee__c> attendee = [SELECT Id FROM Attendee__c];
        GamePlanBLInterface testGamePlan = new GamePlanBLImpl();
        String athleteJsonInput = creaTestJsonInput();
        String testResult = testGamePlan.registerAttendee(athleteJsonInput);
       
    }
    
    @isTest
    static void unsubscribeAthleteFromEvent() {
        athleteRegistered();
        GamePlanBLInterface testGamePlan = new GamePlanBLImpl();
        String athleteJsonInput = creaTestJsonInput();
        String testResult = testGamePlan.updateAthleteInformation(athleteJsonInput);
    }
    
    
}