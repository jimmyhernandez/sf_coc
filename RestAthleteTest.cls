@isTest
private class RestAthleteTest {
    
    static testMethod void createAnAthlete(){
        String jsonInput;
            
        JSONGenerator gen = JSON.createGenerator(true);    
        gen.writeStartObject();
        gen.writeStringField('user_email', 'johndoe@test.ca');
        gen.writeStringField('user_firstname', 'John');
        gen.writeStringField('user_lastname', 'Doe');
        gen.writeEndObject();    
        jsonInput = gen.getAsString();
        String endPoint = '/services/apexrest/RegisterAthlete';
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        
        RestContext.request = req;
        RestContext.response = res;
        
        req.requestURI = endPoint;
        req.httpMethod = 'POST';
        
        req.requestBody = Blob.valueof(jsonInput);
        
        Test.startTest();
        
        String result = RestAthlete.doPost();
        System.debug(result);
        
        Test.stopTest();
        
    }
}