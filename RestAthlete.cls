@RestResource(urlMapping='/RegisterAthlete/*')
global with sharing class RestAthlete {
	@HttpPost
    global static String doPost(){
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        
        String jsonInput = req.requestBody.toString();
        System.debug(jsonInput);
        createAthlete(jsonInput);
        
        return JSON.serialize('Create athlete');
    }
    
    private static Account createAthlete(String jsonInput){
        Map<String, Object> athleteInfo = (Map<String, Object>) JSON.deserializeUntyped(jsonInput);
        
        Account athlete = new Account();
        
        athlete.Name = (String) athleteInfo.get('user_firstname') + ' ' + (String) athleteInfo.get('user_lastname');
        athlete.Email__c = (String) athleteInfo.get('user_email');
        
        insert athlete;
        return athlete;
    }
}