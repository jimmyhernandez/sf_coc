public class GamePlanBLImpl implements GamePlanBLInterface{    
    public String registerAttendee(String athleteJsonInput) {
        Account athlete = new Account();
        String result = 'Test Response from GamePlanBLImpl';
        Map<String, Object> athleteInfo = jsonToSfObjectMap(athleteJsonInput);
        
        String eventName = (String) athleteInfo.get('event_name');
        Events__c gamePlanEventInfo = getGamePlanEvent(eventName);
        
        if ( gamePlanEventInfo.Subscribed__c <= gamePlanEventInfo.Maximum_Attendees__c) {
            if ( hasAthleteRequiredFields(athleteInfo) ) {
                List<Account> athletes = searchAthlete(athleteInfo);
                System.debug(athletes);
                if ( athletes.size() == 0 ) {
                    athlete = createAnAthlete(athleteInfo);
                } else {
                    athlete = athletes.get(0);
                }
                
                Account athleteUpdated = updateAthleteInfo(athlete,athleteInfo);
                result = athleteRegistration(athleteUpdated,gamePlanEventInfo);
            }
        } else {
            result = 'Full';
        }
       
        return result;
    }
    
    public String updateAthleteInformation(String athleteJsonInput) {
        String result = '';
        Map<String, Object> athleteInfo = jsonToSfObjectMap(athleteJsonInput);
        
        String eventName = (String) athleteInfo.get('event_name');
        Events__c gamePlanEventInfo = getGamePlanEvent(eventName);
        
        List<Account> athletes = searchAthlete(athleteInfo);
        if (athletes.size() > 0) {
            Account athlete = removeInfoAthlete(athletes.get(0));
            List<Attendee__c> attendees = unsubscribeAttendee(athlete.Id, gamePlanEventInfo.Id);
            if ( attendees.size() > 0 ) {
                result = attendees.get(0).Id;
            }
        }
        
        return result;
    }
    
    private static List<Attendee__c> unsubscribeAttendee(Id athleteId, Id eventId){
        List<Attendee__c> unsubscribeAttendees = [SELECT Id FROM Attendee__c WHERE Account__c = :athleteId AND Event__c = :eventId LIMIT 1];
        try {
            delete unsubscribeAttendees;
        } catch (Exception e) {
            System.debug(e.getMessage());
        }
        return unsubscribeAttendees;
    }
       
    private static Account removeInfoAthlete(Account athlete){
        athlete.Summit_Status__c = null;
		athlete.Accessibility_Service_Animal__c = null;
        athlete.Accessibility_Transfer_Easily__c = null;
        athlete.Accessibility_Sport_Assistant__c = null;
        athlete.Accessibility_Requirements__c = null;
        athlete.Accommodation_Yes_Text__c = null;
        athlete.Accommodation__c = null;
        athlete.Dietary_Other_Text__c = null;
        athlete.Dietary__c = null;
        athlete.Breakout_1__c = null;
        athlete.Breakout_2__c = null;
        athlete.Breakout_3__c = null;
        athlete.Accessibility_Transfer_Easily_Elaborate__c = null;
        return athlete;
    }
    
    private static String athleteRegistration(Account athlete, Events__c gamePlanEventInfo) {
        String result = '';
        if ( gamePlanEventInfo != null ) {
            Boolean isRegistered = isAthleteRegistered(athlete.Id,gamePlanEventInfo.Id);
            if ( isRegistered ) {
                result = 'Already registered: ';
            } else {
                Attendee__c attendeeRegistration = registerAsAttendee(athlete,gamePlanEventInfo);
                result = 'Attendee Id: ' + attendeeRegistration.Id;
            }
        }
        return result;
    }
    
    private static Boolean hasAthleteRequiredFields(Map<String, Object> athleteInfo) {
        Boolean required = false;
        if ( (String) athleteInfo.get('user_firstname') != '' && (String) athleteInfo.get('user_lastname') != '' && (String) athleteInfo.get('user_email') != '' ) {
            required = true;
        }
        return required;
    }
    
    private Map<String, Object> jsonToSfObjectMap(String athleteJsonInput) {
        Map<String, Object> jsonDeserialize = (Map<String, Object>) JSON.deserializeUntyped(athleteJsonInput);
        return jsonDeserialize;
    }
    
    private static List<Account> searchAthlete(Map<String, Object> athleteInfo) {
        String athleteName = (String) athleteInfo.get('user_firstname') + ' ' + (String) athleteInfo.get('user_lastname');
        String email = (String) athleteInfo.get('user_email');
        List<Account> athletes = [SELECT Id, 
                           	Name,
                           	Email__c,
                           	Summit_Status__c, 
                           	Accessibility_Service_Animal__c,
                           	Accessibility_Transfer_Easily__c,
                            Accessibility_Transfer_Easily_Elaborate__c,
                           	Accessibility_Sport_Assistant__c,
                           	Accessibility_Requirements__c,
                           	Accommodation_Yes_Text__c,
                           	Accommodation__c,
                           	Dietary_Other_Text__c,
                           	Dietary__c,
                           	Breakout_1__c,
                           	Breakout_2__c,
                           	Breakout_3__c
                           FROM Account
                           WHERE Name = :athleteName];
        return athletes;
    }
    
    private static Events__c getGamePlanEvent(String eventName) {
        List<Events__c> gamePlanEvent = [SELECT Id, Name, Subscribed__c, Maximum_Attendees__c FROM Events__c WHERE Name = :eventName];
        if ( gamePlanEvent.size() > 0 ) {
            return gamePlanEvent.get(0);
        }
        return null;
    }
    
    private static Boolean isAthleteRegistered(Id athleteId, Id eventId) {
        Boolean isAlreadyRegistered = false;
        List<Attendee__c > attendees = [SELECT Id FROM Attendee__c WHERE Account__c = :athleteId AND Event__c = :eventId];
        if (attendees.size() > 0 ) {
            isAlreadyRegistered = true;
        }
        return isAlreadyRegistered;
    }
    
    private static Attendee__c registerAsAttendee(Account acc, Events__c event) {
        Attendee__c athleteAttendee = new Attendee__c(Account__c = acc.Id, Event__c = event.Id);
        insert athleteAttendee;
        return athleteAttendee;
    }
    
    private static Account updateAthleteInfo(Account athlete, Map<String, Object> athleteInfo){
        Account updateAthleteInfo = mappingAthleteFields(athlete,athleteInfo);
        update updateAthleteInfo;
        return updateAthleteInfo;
    }
    
    private static Account createAnAthlete(Map<String, Object> athleteInfo) {
        String fullName = (String) athleteInfo.get('user_firstname') + ' ' + (String) athleteInfo.get('user_lastname');
        Account newAthlete = new Account();
        newAthlete.Name = fullName;
        insert newAthlete;
        return newAthlete;
    }
    
    private static Account mappingAthleteFields(Account athlete, Map<String, Object> athleteInfo){
        athlete.Email__c = (String) athleteInfo.get('user_email');
        athlete.Summit_Status__c = (String) athleteInfo.get('status');
		athlete.Accessibility_Service_Animal__c = (String) athleteInfo.get('accessibility_service_animal');
        athlete.Accessibility_Transfer_Easily__c = (String) athleteInfo.get('accessibility_transfer_easily');
        athlete.Accessibility_Sport_Assistant__c = (String) athleteInfo.get('accessibility_sport_assistant');
        athlete.Accessibility_Requirements__c = (String) athleteInfo.get('accessibility_requirements');
        athlete.Accommodation_Yes_Text__c = (String) athleteInfo.get('accommodation_yes_text');
        athlete.Accommodation__c = (String) athleteInfo.get('accommodation');
        athlete.Dietary_Other_Text__c = (String) athleteInfo.get('dietary_other_text');
        athlete.Dietary__c = (String) athleteInfo.get('dietary');
        athlete.Breakout_1__c = (String) athleteInfo.get('breakout_1');
        athlete.Breakout_2__c = (String) athleteInfo.get('breakout_2');
        athlete.Breakout_3__c = (String) athleteInfo.get('breakout_3');
        athlete.Accessibility_Transfer_Easily_Elaborate__c = (String) athleteInfo.get('accessibility_transfer_easily_elaborate');
        athlete.Terms_and_Conditions__c = (String) athleteInfo.get('terms_and_conditions') == 'Yes' ? true : false;
        athlete.Athlete_Coach_Type__c = (String) athleteInfo.get('athlete_coach_type');
        return athlete;
    }
}