@isTest
private class GamePlanTest {
    
    private static String createTestJsonInput() {
        JSONGenerator gen = JSON.createGenerator(true);    
        gen.writeStartObject();
        gen.writeStringField('user_email', 'testuser101@test.ca');
        gen.writeStringField('user_firstname', 'John');
        gen.writeStringField('user_lastname', 'Doe');
        gen.writeStringField('status', 'Test Summit Status');
        gen.writeStringField('accessibility_service_animal','Test Accessibility Service Animal');
        gen.writeStringField('accessibility_transfer_easily','Test Accessibility Transfer Easily');
        gen.writeStringField('accessibility_sport_assistant','Test Accessibility Sport Assistant');
        gen.writeStringField('accessibility_requirements','Test Accessibility Requirements');
        gen.writeStringField('accommodation_yes_text','Test Accomodation Yest Text');
        gen.writeStringField('accommodation','Test My Accomodation');
        gen.writeStringField('dietary_other_text','Test Dietary Other Text');
        gen.writeStringField('dietary','Test Dietary');
        gen.writeStringField('breakout_1','Test Breakout One');
        gen.writeStringField('breakout_2','Test Breakout Two');
        gen.writeStringField('breakout_3','Test Breakout Three');
        gen.writeStringField('breakout_3','Test Breakout Three');
        gen.writeStringField('accessibility_transfer_easily_elaborate','Test Accessibility');
        gen.writeStringField('event_name','GamePlanSummit2018');
        gen.writeEndObject();    
        return gen.getAsString();
    }
    
    @testSetup 
    static void setup() {
		Account testAthlete = new Account(name = 'John Doe', Email__c  = 'testuser101@test.ca');
        insert testAthlete;
        
        Events__c testEvent = new Events__c(Name = 'GamePlanSummit2018', Maximum_Attendees__c = 160);
        insert testEvent;
        
	}
    
    static testMethod void eventRegistration(){        

        String endPoint = '/services/apexrest/GamePlanSummit2018';
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        
        String JsonMsg = createTestJsonInput();
        
        req.requestURI = endPoint;
        req.httpMethod = 'POST';
        req.requestBody = Blob.valueof(JsonMsg);
        
        RestContext.request = req;
        RestContext.response = res;
        
        Test.startTest();
        String result = rest_api.doPost();
        System.debug('Test Result: ' + result);        
        Test.stopTest();
        
    }
    
    static testMethod void unsubscribeAthleteFromEvent(){        

        String endPoint = '/services/apexrest/GamePlanSummit2018';
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        
        String JsonMsg = createTestJsonInput();
        
        req.requestURI = endPoint;
        req.httpMethod = 'PUT';
        req.requestBody = Blob.valueof(JsonMsg);
        
        RestContext.request = req;
        RestContext.response = res;
        
        Test.startTest();
        String result = rest_api.updateAthlete();
        System.debug('Test Result: ' + result);        
        Test.stopTest();
        
    }
}