public interface GamePlanBLInterface {
	String registerAttendee(String athleteJsonInput);
    String updateAthleteInformation(String athleteJsonInput);
}